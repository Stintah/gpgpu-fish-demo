import test_data from "../data/test-data.json";

export interface IData {
    param_0: number;
    param_1: number;
    param_2: number;
    param_3: number;
    param_4: number;
    param_5: number;
    param_6: number;
}

export default class DataFeed {
    onData: (data: any) => void;

    constructor(onData: (data: any) => void) {
        this.onData = onData;
        this.fetchTestData();
    }

    start = () => {};

    fetchTestData = () => {
        setTimeout(() => {
            console.log(test_data);
            const extremes = test_data.extremes;
            let high_extreme = 0;
            let low_extreme = 0;
            for (const extreme of extremes) {
                if (extreme.state === "HIGH TIDE") {
                    if (extreme.height > high_extreme) {
                        high_extreme = extreme.height;
                    }
                } else if (extreme.state === "LOW TIDE") {
                    if (extreme.height < low_extreme) {
                        low_extreme = extreme.height;
                    }
                }
            }
            const latest_height = test_data.heights[test_data.heights.length - 1].height;
            console.log(latest_height, high_extreme, low_extreme);
            const latest_height_normalized = (latest_height + Math.abs(low_extreme)) / (high_extreme + Math.abs(low_extreme));
            console.log(latest_height_normalized);
            this.fetchTestData();
        }, 5000);
    };

    fetchData = () => {
        fetch("https://tides.p.rapidapi.com/tides?longitude=-2.097&latitude=44.414&interval=60&duration=1440", {
            method: "GET",
            headers: {
                "x-rapidapi-host": "tides.p.rapidapi.com",
                "x-rapidapi-key": "8553d4e044msh6ce849ddffb0c9dp1ac81fjsn4dc7417dff7c",
            },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                setTimeout(() => {
                    this.fetchData();
                    //this.onData.bind(this)(response);
                }, 30000);
            })
            .catch((err) => {
                console.error(err);
            });
    };
}
