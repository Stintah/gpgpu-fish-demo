import "../css/main.css";
import Content from "./content";
let index = 0;
class App {
    private _content: Content;

    constructor(content: Content) {
        this._content = content;
    }

    public setup(): void {
        // run setup in content then trigger update loop, maybe with callback once content is ready
        this._content.setup(() => this.update());
    }

    private update(): void {
        // need to bind the current this reference to the callback
        requestAnimationFrame(this.update.bind(this));

        this._content.update();
    }
}

window.onload = () => {
    const app = new App(new Content());
    app.setup();
};
