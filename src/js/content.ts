import DataFeed from "./data-feed";
import WglBaseScene from "./wgl/scene/wgl-base-scene";
import WglFlockingFish from "./wgl/scene/wgl-scene-flocking-fish";
import { WebGLRenderer, WebGLRendererParameters } from "three";

export default class Content {
    private canvas: HTMLCanvasElement;
    private context: WebGL2RenderingContext;
    private height: number = document.body.clientHeight;
    private width: number = document.body.clientWidth;
    private data_feed: DataFeed;
    private renderer_parameters: WebGLRendererParameters;
    private renderer: WebGLRenderer;
    private wgl_scene: WglBaseScene | null = null;

    constructor() {
        this.canvas = <HTMLCanvasElement>document.createElement("canvas");
        document.body.appendChild(this.canvas);
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.context = this.canvas.getContext("webgl2");
        this.data_feed = new DataFeed((data) => console.log(data));
        this.renderer_parameters = {
            canvas: this.canvas,
            context: this.context,
            alpha: false,
            antialias: true,
            powerPreference: "high-performance",
        };
        this.renderer = new WebGLRenderer(this.renderer_parameters);
    }

    setup = (onReady: () => void) => {
        //this.wgl_scene = new WglSceneVideo("scene01", this.width, this.height, this.renderer, onReady, "./videos/test-17.mp4", shader02);
        this.wgl_scene = new WglFlockingFish("Flocking Fish", this.width, this.height, this.renderer, onReady);
        //onReady();
    };

    update = () => {
        if (this.wgl_scene) {
            this.wgl_scene.update();
        }
        //console.log("updating");
    };
}
