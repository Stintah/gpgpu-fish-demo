//--------------------------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------------------------

import { Camera, Object3D, Vector2, Vector3, Sprite, Matrix4, Quaternion } from "three";

//--------------------------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------------------------

export class WglUtils {
    //toScreen variables
    private static vector: Vector3;
    private static widthHalf: number;
    private static screenVector: Vector2;

    // move towards with ease variables
    private static diff: number;
    private static move: number;
    //

    constructor() {
        //toScreen
        WglUtils.vector = new Vector3();
        WglUtils.widthHalf = 0;
        WglUtils.screenVector = new Vector2();
        // move towards with ease variables
        WglUtils.diff = 0;
        WglUtils.move = 0;
        //
    }

    static toScreen(object: Object3D | Sprite, camera: Camera, width: number, height: number) {
        const vector = new Vector3();
        vector.setFromMatrixPosition(object.matrixWorld);
        vector.project(camera);
        const widthHalf = width * 0.5,
            heightHalf = height * 0.5;
        vector.x = vector.x * widthHalf + widthHalf;
        vector.y = -(vector.y * heightHalf) + heightHalf;

        return new Vector2(vector.x, vector.y);
    }

    static toScreenVector3(vector3: Vector3, rotation: Quaternion, camera: Camera, width: number, height: number) {
        const vector = new Vector3();
        const scale = new Vector3(1, 1, 1);
        const mat4 = new Matrix4().compose(vector3, rotation, scale);
        vector.setFromMatrixPosition(mat4);
        vector.project(camera);
        const widthHalf = width * 0.5,
            heightHalf = height * 0.5;
        vector.x = vector.x * widthHalf + widthHalf;
        vector.y = -(vector.y * heightHalf) + heightHalf;

        return new Vector2(vector.x, vector.y);
    }

    static iterateArray = (currentIndex: number, array: []) => {
        currentIndex++;
        if (currentIndex >= array.length) {
            currentIndex = 0;
        }
        return currentIndex;
    };

    static lerp = (current: number, target: number, t: number) => {
        return current + (target - current) * t;
    };

    static linear(t: number) {
        return t;
    }

    static easeInOutQuad(t: number) {
        return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
    }

    static easeInOutCubic(t: number) {
        return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
    }

    static easeInOutQuart(t: number) {
        return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t;
    }

    static animate({ timing, draw, duration, onComplete }: { timing: Function; draw: Function; duration: number; onComplete: () => void }) {
        let start = performance.now();
        requestAnimationFrame(function animate(time) {
            // timeFraction goes from 0 to 1
            let timeFraction = (time - start) / duration;
            if (timeFraction > 1) timeFraction = 1;

            // calculate the current animation state
            let progress = timing(timeFraction);

            draw(progress); // draw it

            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            } else {
                onComplete();
            }
        });
    }

    static findDifference(a: number, b: number): number {
        return Math.abs(a - b);
    }

    static findAverage(array: number[]) {
        let average = array.reduce((accumulator, currentValue) => {
            return accumulator + currentValue / array.length;
        }, 0);
        return average;
    }

    static findAverageVector3(array: Vector3[]) {
        let sum = new Vector3();
        let average = new Vector3();
        for (let i = 0; i < array.length; i++) {
            sum.add(array[i]);
        }
        average = sum.divideScalar(array.length);
        return average;
    }

    static findAverageVector2(array: Vector2[]) {
        let sum = new Vector2();
        let average = new Vector2();
        for (let i = 0; i < array.length; i++) {
            sum.add(array[i]);
        }
        average = sum.divideScalar(array.length);
        return average;
    }

    static moveTowards(current: number, target: number, maxDelta: number): number {
        if (Math.abs(target - current) <= maxDelta) {
            return target;
        }
        return current + Math.sign(target - current) * maxDelta;
    }

    static moveTowardsWithEase(current: number, target: number, maxDelta: number) {
        this.diff = WglUtils.findDifference(current, target);
        this.move = WglUtils.moveTowards(current, target, maxDelta * this.diff);
        return this.move;
    }

    static clamp = (num: number, min: number, max: number) => {
        return num <= min ? min : num >= max ? max : num;
    };

    static remove(parent: Object3D, object: Object3D) {
        parent.remove(object);
    }

    static getDecimal = (n: number) => {
        return n - Math.floor(n);
    };

    static removeDuplicateEntries(array: any[]) {
        let newArray = array.filter((e, i, a) => a.indexOf(e) === i);
        return newArray;
    }

    static convertToNormalizedScreenCoords(screenCoordsX: number, screenCoordsY: number, value: Vector3 | Vector2, canvas: HTMLCanvasElement, offsetTop: number) {
        value.setX((screenCoordsX / canvas.width - canvas.offsetLeft) * 2 - 1);
        value.setY((((screenCoordsY - offsetTop) / canvas.height) * 2 - 1) * -1);
    }

    static screenToWorld(camera: Camera, screenPosition: Vector2, width: number, height: number, pos: Vector3) {
        const emptyVec3 = new Vector3();
        emptyVec3.set((screenPosition.x / width) * 2 - 1, (screenPosition.y / height) * 2 - 1, 0.5);
        emptyVec3.unproject(camera);
        emptyVec3.sub(camera.position).normalize();
        const distance = -camera.position.z / emptyVec3.z;
        //pos.copy(camera.position).add(emptyVec3.multiplyScalar(distance));
        return pos.copy(camera.position).add(emptyVec3.multiplyScalar(distance));
    }

    static screenToWorld2(camera: Camera, screenPosition: Vector2, width: number, height: number, pos: Vector3, canvas: HTMLCanvasElement, offsetTop: number) {
        const emptyVec3 = new Vector3();
        //emptyVec3.set((screenPosition.x / width) * 2 - 1, (screenPosition.y / height) * 2 - 1, 0.5);
        WglUtils.convertToNormalizedScreenCoords(screenPosition.x, screenPosition.y, emptyVec3, canvas, offsetTop);
        const vector = new Vector3(emptyVec3.x, emptyVec3.y, -1).unproject(camera);
        //emptyVec3.unproject(camera);
        vector.sub(camera.position).normalize();
        const distance = -camera.position.z / vector.z;
        //pos.copy(camera.position).add(emptyVec3.multiplyScalar(distance));
        return pos.copy(camera.position).add(vector.multiplyScalar(distance));
    }

    static screenToWorld3(camera: Camera, screenPosition: Vector2, width: number, height: number, pos: Vector3) {
        const emptyVec3 = new Vector3();
        emptyVec3.set((screenPosition.x / width) * 2 - 1, (screenPosition.y / height) * 2 - 1, 0.5);
        emptyVec3.unproject(camera);
        emptyVec3.sub(camera.position).normalize();
        const distance = -camera.position.z / emptyVec3.z;
        //pos.copy(camera.position).add(emptyVec3.multiplyScalar(distance));
        return pos.copy(camera.position).add(emptyVec3.multiplyScalar(distance));
    }

    static getSingleUserChannelValue = (object3D: Object3D, channelName: string) => {
        let channels = object3D.userData.channels;
        for (let key in channels) {
            let channel = channels[key];
            if (channel.name === channelName) {
                return channel.value;
            }
        }
    };

    static hasSingleUserChannelName = (object3D: Object3D, channelName: string) => {
        let channels = object3D.userData.channels;
        for (let key in channels) {
            let channel = channels[key];
            if (channel.name === channelName) {
                return true;
            }
        }
        return false;
    };

    static signedAngle = (v0: Vector3, v1: Vector3) => {
        let angle = Math.acos(v0.dot(v1));
        let cross = new Vector3().crossVectors(v0, v1);
        const N = new Vector3(0, 1, 0);
        if (N.dot(cross) < 0) {
            // Or > 0
            angle = -angle;
        }
        return angle;
    };

    static nextPowerOf2 = (n: number) => {
        return Math.pow(2, Math.ceil(Math.log(n) / Math.log(2)));
    };

    static isSafari = () => {
        return !!navigator.userAgent.match(/Safari/i) && !navigator.userAgent.match(/Chrome/i);
    };
}
