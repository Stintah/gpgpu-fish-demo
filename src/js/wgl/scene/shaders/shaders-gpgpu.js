//--------------------------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------------------------

const shader_position = {
    fragmentShader: `
   
    uniform float time;
    uniform float delta;
	uniform float speed;
	
	const float WIDTH = sim_width;

    void main()	{

        vec2 uv = gl_FragCoord.xy / vec2(WIDTH, WIDTH);
        vec4 tmpPos = texture2D( texturePosition, uv );
        vec3 position = tmpPos.xyz;
        vec3 velocity = texture2D( textureVelocity, uv ).xyz;

        float phase = tmpPos.w;

        phase = mod( ( phase + delta +
            length( velocity.xz ) * delta * 3. +
            max( velocity.y, 0.0 ) * delta * 6. ), 62.83 );

        gl_FragColor = vec4( position + velocity * delta * speed , phase );
   
       }`,
};

const shader_velocity = {
    fragmentShader: `
   
    uniform float time;
	uniform float testing;
	uniform float delta; // about 0.016
	uniform float separationDistance; // 20
	uniform float alignmentDistance; // 40
	uniform float cohesionDistance; //
	uniform float freedomFactor;
	uniform float target_x;
	uniform vec3 predator;


	const float PI = 3.141592653589793;
	const float PI_2 = PI * 2.0;
	// const float VISION = PI * 0.55;

	float zoneRadius = 10.0;
	float zoneRadiusSquared = 100.0;

	float separationThresh = 0.45;
	float alignmentThresh = 0.65;

	const float WIDTH = sim_width;

	const float UPPER_BOUNDS = bounds;
	const float LOWER_BOUNDS = -UPPER_BOUNDS;

	const float SPEED_LIMIT = 5.0;

	float rand( vec2 co ){
		return fract( sin( dot( co.xy, vec2(12.9898,78.233) ) ) * 43758.5453 );
	}

	float bias (float time, float bias) {
        return time / ((1.0 / bias - 2.0) * (1.0 - time) + 1.0); 
    }

    float gain (float time, float gain) {
        if (time < 0.5) {
            return bias(time * 2.0, gain) / 2.0;
        } else {
            return bias(time * 2.0 - 1.0, 1.0 - gain) / 2.0 + 0.5;
        }
    }

	vec3 addDragForce (float coefficient, vec3 velocity) {
        vec3 drag = normalize( vec3(velocity) ) * -1.0;
		float speed = length( velocity );
		float speed_squared = speed * speed;
        float magnitude = coefficient * speed_squared;
        drag *= magnitude;
        return drag;
    }

    vec3 gravitationalForce ( vec3 position, vec3 force_position,  float force_mass ) {
        vec3 direction = position - force_position;
        float distance = length( direction );
        float distance_squared = distance * distance;
        float g = 1.0;

        float strength = ( g - (1.0 * force_mass) ) / distance_squared;

        return direction * (strength * 1.0 );
    }

    vec3 addGravitationalForce ( vec3 position, vec3 force_position, float force_mass, float distance_threshold ) {
        float distance = abs(length(position - force_position));
        if (distance < distance_threshold) {
            float distance_normalized = distance / distance_threshold;
            float curve = gain(bias(distance_normalized, 0.25), 0.6);
            //return gravitationalForce(position, force_position, force_mass) * curve;
            return gravitationalForce(position, force_position, force_mass) * curve;
        }
    }

	

	void main() {

		zoneRadius = separationDistance + alignmentDistance + cohesionDistance;
		separationThresh = separationDistance / zoneRadius;
		alignmentThresh = ( separationDistance + alignmentDistance ) / zoneRadius;
		zoneRadiusSquared = zoneRadius * zoneRadius;

		vec2 uv = gl_FragCoord.xy / vec2(WIDTH, WIDTH);
		vec3 birdPosition, birdVelocity;

		vec3 selfPosition = texture2D( texturePosition, uv ).xyz;
		vec3 selfVelocity = texture2D( textureVelocity, uv ).xyz;

		float dist;
		vec3 dir; // direction
		float distSquared;

		float separationSquared = separationDistance * separationDistance;
		float cohesionSquared = cohesionDistance * cohesionDistance;

		float f;
		float percent;

		vec3 velocity = selfVelocity;

		float limit = SPEED_LIMIT;

		vec3 acc = vec3(0.0);

		//predator
		vec3 newPredator = vec3(predator.x, predator.y, 0.0);
		dir = newPredator * UPPER_BOUNDS - selfPosition;
		//dir.z = 0.0;
		//dir.z *= 0.1;
		dist = length( dir );
		distSquared = dist * dist;

		float preyRadius = 50.0;
		float preyRadiusSq = preyRadius * preyRadius;

		// move birds away from predator
		if ( dist < preyRadius ) {
			//f = ( distSquared / preyRadiusSq ) * delta * 10.0;
			//velocity -= normalize( dir ) * f;
			//limit += (SPEED_LIMIT*4.0);
		}


		// Attract flocks to the center
		vec3 central = vec3( 0., 0., 0. );
		dir = selfPosition - central;
		dist = length( dir );

		//dir.y *= 2.5;
		//acc -= normalize( dir ) * delta * 1.0;

		for ( float y = 0.0; y < WIDTH; y++ ) {
			for ( float x = 0.0; x < WIDTH; x++ ) {

				vec2 ref = vec2( x + 0.5, y + 0.5 ) / vec2(WIDTH, WIDTH);
				birdPosition = texture2D( texturePosition, ref ).xyz;
				birdVelocity = texture2D( textureVelocity, ref ).xyz;

				vec3 new_dir = selfPosition - birdPosition;


				float angle = abs(dot(normalize(-new_dir), normalize( velocity)));

				if (angle > PI * 0.25 ) continue;
					dir = birdPosition - selfPosition;
					dist = length( dir );

					if ( dist < 0.001 ) continue;

					distSquared = dist * dist;

					if ( distSquared > zoneRadiusSquared ) continue;

					percent = distSquared / zoneRadiusSquared;

					if ( percent < separationThresh ) { // low

						// Separation - Move apart for comfort
						f = (( separationThresh / percent - 1.0 ) * delta);
						acc -= normalize( dir ) * f ;

					} else if ( percent < alignmentThresh ) { // high

						// Alignment - fly the same direction
						float threshDelta = alignmentThresh - separationThresh;
						float adjustedPercent = ( percent - separationThresh ) / threshDelta;

						f = (( 0.5 - cos( adjustedPercent * PI_2 ) * 0.5 + 0.5 ) * delta);
						acc += normalize( birdVelocity ) * f;

					} else {

						// Attraction / Cohesion - move closer
						float threshDelta = 1.0 - alignmentThresh;
						float adjustedPercent;
						if( threshDelta == 0. ) adjustedPercent = 1.;
						else adjustedPercent = ( percent - alignmentThresh ) / threshDelta;

						f = (( 0.5 - ( cos( adjustedPercent * PI_2 ) * -0.5 + 0.5 ) ) * delta);

						acc += normalize( dir ) * f;
					}
				}
		}

		acc += addGravitationalForce( selfPosition, vec3(0.0, 0.0, target_x), 500.0, 750.0);

		//acc += addDragForce( 0.5, velocity );

		velocity += acc;

		acc = vec3(0.0,0.0,0.0);

		// this make tends to fly around than down or up
		if (velocity.y > 0.) velocity.y *= (1.0 - 0.2 * delta);

		// Speed Limits
		if ( length( velocity ) > limit ) {
			velocity = normalize( velocity ) * limit;
			
		}

		//velocity.y *= 0.5;

		

		gl_FragColor = vec4( velocity, 1.0 );

			}`,
};

const shader_position_02 = {
    fragmentShader: `
   
    uniform float time;
    uniform float delta;

	const float upper_bounds = bounds;
	const float lower_bounds = -upper_bounds;
	
	const float width = texture_sim_width;

    void main()	{

        vec2 uv = gl_FragCoord.xy / vec2(width, width);
        vec4 t_pos = texture2D( texture_position, uv );
        vec3 position = t_pos.xyz;
        vec3 velocity = texture2D( texture_velocity, uv ).xyz;

        float phase = t_pos.w;

		//wrap position
		if (position.x > upper_bounds) {
			position.x = lower_bounds;
		} else if (position.x < lower_bounds) {
			position.x = upper_bounds;
		}
		if (position.y > upper_bounds) {
			position.y = lower_bounds;
		} else if (position.y < lower_bounds) {
			position.y = upper_bounds;
		}
		if (position.z > upper_bounds) {
			position.z = lower_bounds;
		} else if (position.z < lower_bounds) {
			position.z = upper_bounds;
		}


        gl_FragColor = vec4( position + velocity * delta * 1.0 , phase );
   
       }`,
};

const shader_velocity_02 = {
    fragmentShader: `
   
    uniform float time;
	uniform float delta;
	uniform float x;
    uniform float y;
    uniform float z;
	
	const float PI = 3.141592653589793;
	const float PI_2 = PI * 2.0;
	
	const float width = texture_sim_width;

	const float upper_bounds = bounds;
	const float lower_bounds = -upper_bounds;

	const float mass = 1.0;
	vec3 acc = vec3(0.0);
    vec3 vel;
	vec3 pos;

	float rand( vec2 co ) {
		return fract( sin( dot( co.xy, vec2(12.9898,78.233) ) ) * 43758.5453 );
	}

	float bias (float time, float bias) {
        return time / ((1.0 / bias - 2.0) * (1.0 - time) + 1.0); 
    }

    float gain (float time, float gain) {
        if (time < 0.5) {
            return bias(time * 2.0, gain) / 2.0;
        } else {
            return bias(time * 2.0 - 1.0, 1.0 - gain) / 2.0 + 0.5;
        }
    }

	vec3 addDragForce (float coefficient, vec3 velocity) {
        vec3 drag = normalize( vec3(velocity) ) * -1.0;
		float speed = length( velocity );
		float speed_squared = speed * speed;
        float magnitude = coefficient * speed_squared;
        drag *= magnitude;
        return drag;
    }

    vec3 gravitationalForce ( vec3 position, vec3 force_position,  float force_mass ) {
        vec3 direction = position - force_position;
        float distance = length( direction );
        float distance_squared = distance * distance;
        float g = 1.0;

        float strength = ( g - (mass * force_mass) ) / distance_squared;

        return direction * (strength * 1.0 );
    }

    vec3 addGravitationalForce ( vec3 position, vec3 force_position, float force_mass, float distance_threshold ) {
        float distance = abs(length(position - force_position));
        if (distance < distance_threshold) {
            float distance_normalized = distance / distance_threshold;
            float curve = gain(bias(distance_normalized, 0.25), 0.6);
            //return gravitationalForce(position, force_position, force_mass) * curve;
            return gravitationalForce(position, force_position, force_mass) * curve;
        }
    }

	void main() {

		vec2 uv = gl_FragCoord.xy / vec2(width, width);
		vec3 velocity = texture2D( texture_velocity, uv ).xyz;

		pos = texture2D( texture_position, uv ).xyz;

		vec3 a = vec3(x,y,z);

		acc += addGravitationalForce( pos, a, 1.25, 16.0);

		acc += addDragForce( 0.00025, velocity );

		velocity += acc;

		acc = vec3(0.0,0.0,0.0);

		gl_FragColor = vec4( velocity , 1.0 );
	}`,
};

const shader_velocity_03 = {
    fragmentShader: `
   
    uniform float time;
	uniform float delta;
	uniform float x;
    uniform float y;
    uniform float z;
	
	const float PI = 3.141592653589793;
	const float PI_2 = PI * 2.0;
	
	const float width = texture_sim_width;

	const float upper_bounds = bounds;
	const float lower_bounds = -upper_bounds;

	const float mass = 8.0;
	vec3 acc = vec3(0.0);
    vec3 vel;
	vec3 pos;

	vec3 other_position;
	vec3 other_velocity;

	float rand( vec2 co ) {
		return fract( sin( dot( co.xy, vec2(12.9898,78.233) ) ) * 43758.5453 );
	}

	float bias (float time, float bias) {
        return time / ((1.0 / bias - 2.0) * (1.0 - time) + 1.0); 
    }

    float gain (float time, float gain) {
        if (time < 0.5) {
            return bias(time * 2.0, gain) / 2.0;
        } else {
            return bias(time * 2.0 - 1.0, 1.0 - gain) / 2.0 + 0.5;
        }
    }

	vec3 addDragForce (float coefficient, vec3 velocity) {
        vec3 drag = normalize( vec3(velocity) ) * -1.0;
		float speed = length( velocity );
		float speed_squared = speed * speed;
        float magnitude = coefficient * speed_squared;
        drag *= magnitude;
        return drag;
    }

    vec3 gravitationalForce ( vec3 position, vec3 force_position,  float force_mass ) {
        vec3 direction = position - force_position;
        float distance = length( direction );
        float distance_squared = distance * distance;
        float g = 1.0;

        float strength = ( g - (mass * force_mass) ) / distance_squared;

        return direction * (strength * 1.0 );
    }

    vec3 addGravitationalForce ( vec3 position, vec3 force_position, float force_mass, float distance_threshold ) {
        float distance = abs(length(position - force_position));
        if (distance < distance_threshold) {
            float distance_normalized = distance / distance_threshold;
            float curve = gain(bias(distance_normalized, 0.25), 0.6);
            //return gravitationalForce(position, force_position, force_mass) * curve;
            return gravitationalForce(position, force_position, force_mass) * curve;
        }
    }

	void main() {

		vec2 uv = gl_FragCoord.xy / vec2(width, width);
		vec3 velocity = texture2D( texture_velocity, uv ).xyz;

		pos = texture2D( texture_position, uv ).xyz;

		vec3 a = vec3(x,y,z);

		for ( float y = 0.0; y < width; y++ ) {
			for ( float x = 0.0; x < width; x++ ) {

				vec2 ref = vec2( x + 0.5, y + 0.5 ) / vec2(width, width);
				other_position = texture2D( texture_position, ref ).xyz;
				other_velocity = texture2D( texture_velocity, ref ).xyz;

				float dist = abs(length(pos - other_position));

				if ( dist < 7.0 ) continue;
				acc += addGravitationalForce( pos, other_position, 0.01, 8.0);

				
				
			}
		}

		//acc += addGravitationalForce( pos, a, 10.25, 16.0);

		//acc += addDragForce( 0.0005, velocity );

		acc += addDragForce( 0.01, velocity );

		velocity += acc;

		acc = vec3(0.0,0.0,0.0);

		gl_FragColor = vec4( velocity, 1.0 );
	}`,
};

export { shader_position, shader_velocity, shader_position_02, shader_velocity_02, shader_velocity_03 };
