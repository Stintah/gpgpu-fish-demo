//--------------------------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------------------------

const shader01 = {
    uniforms: {
        tDiffuse: {
            value: null,
        },
        param_0: {
            value: 1.0,
        },
        param_1: {
            value: 1.0,
        },
        param_2: {
            value: 1.0,
        },
        param_3: {
            value: 1.0,
        },
        param_4: {
            value: 1.0,
        },
        param_5: {
            value: 1.0,
        },
        param_6: {
            value: 1.0,
        },
    },

    vertexShader: `

    varying vec2 vUv;
    
    void main() 
    {
        vUv = vec2( uv.x, uv.y );
        
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }`,

    fragmentShader: `
     
    uniform float param_0;
    uniform float param_1;
    uniform float param_2;
    uniform float param_3;
    uniform float param_4;
    uniform float param_5;
    uniform float param_6;

    uniform sampler2D tDiffuse;
  
    varying vec2 vUv;
    
    void main() {

        vec4 pass_0 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25) + 0.75));
        vec4 pass_1 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25) + 0.5) );
        vec4 pass_2 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25) + 0.25));
        vec4 pass_3 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25)) );

        vec3 ambient_color = vec3(0.01,0.01,0.01);

        vec3 color_01 = vec3(1.0,0.0,0.0);
        vec3 color_02 = vec3(0.0,1.0,0.0);
        vec3 color_03 = vec3(0.0,0.0,1.0);
        
        vec3 sun_01_blend_01 = mix(pass_1.r*color_01, pass_1.g*color_01, clamp(param_1*2.0, 0.0, 1.0));
        vec3 sun_01_blend_02 = mix(sun_01_blend_01, pass_1.b*color_01, clamp(param_1*2.0 - 1.0, 0.0, 1.0));

        vec3 sun_02_blend_01 = mix(pass_2.r*color_02, pass_2.g*color_02, clamp(param_2*2.0, 0.0, 1.0));
        vec3 sun_02_blend_02 = mix(sun_02_blend_01, pass_2.b*color_02, clamp(param_2*2.0 - 1.0, 0.0, 1.0));

        vec3 sun_03_blend_01 = mix(pass_3.r*color_03, pass_3.g*color_03, clamp(param_3*2.0, 0.0, 1.0));
        vec3 sun_03_blend_02 = mix(sun_03_blend_01, pass_3.b*color_03, clamp(param_3*2.0 - 1.0, 0.0, 1.0));
        
        vec3 result = pass_0.rgb + (sun_01_blend_02 * param_4) + (sun_02_blend_02 * param_5) + (sun_03_blend_02 * param_6);

        gl_FragColor = vec4(result, 1.0);

    }`,
};

const shader02 = {
    uniforms: {
        tDiffuse: {
            value: null,
        },
        param_0: {
            value: 1.0,
        },
        param_1: {
            value: 1.0,
        },
        param_2: {
            value: 1.0,
        },
        param_3: {
            value: 1.0,
        },
        param_4: {
            value: 1.0,
        },
        param_5: {
            value: 1.0,
        },
        param_6: {
            value: 1.0,
        },
    },

    vertexShader: `

    varying vec2 vUv;
    
    void main() 
    {
        vUv = vec2( uv.x, uv.y );
        
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }`,

    fragmentShader: `
     
    uniform float param_0;
    uniform float param_1;
    uniform float param_2;
    uniform float param_3;
    uniform float param_4;
    uniform float param_5;
    uniform float param_6;

    uniform sampler2D tDiffuse;
  
    varying vec2 vUv;

    vec3 getCompColor(vec3 input_color) {
        return vec3(1.0 - input_color.r, 1.0 - input_color.g, 1.0 - input_color.b );
    }

    vec3 getNormalizedColor(vec3 input_color) {
        return vec3(input_color.r/255.0, input_color.g/255.0, input_color.b/255.0);
    }
    
    void main() {

        vec4 pass_0 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25) + 0.75));
        vec4 pass_1 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25) + 0.5) );
        vec4 pass_2 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25) + 0.25));
        vec4 pass_3 = texture2D( tDiffuse, vec2(vUv.r, (vUv.g * 0.25)) );

        vec3 ambient_color = vec3(0.01,0.01,0.01);

        vec3 col_0_hl = getNormalizedColor(vec3(196.0, 214.0, 255.0)) * 0.125;
        vec3 col_0_s = getCompColor(col_0_hl) * 0.125;
        col_0_s = vec3(0.0);

        vec3 col_1_hl = getNormalizedColor(vec3(255.0, 177.0, 110.0));
        vec3 col_1_s = getCompColor(col_1_hl) * 0.25;
        col_1_s = vec3(0.0);

        vec3 col_2_hl = getNormalizedColor(vec3(255.0, 206.0, 166.0));
        vec3 col_2_s = getCompColor(col_2_hl) * 0.5;
        col_2_s = vec3(0.0);


        vec3 col_3_hl = getNormalizedColor(vec3(255.0, 228.0, 206.0));
        vec3 col_3_s = getCompColor(col_3_hl) * 0.75;
        col_3_s = vec3(0.0);

        vec3 col_4_hl = getNormalizedColor(vec3(255.0,254.0, 250.0));
        vec3 col_4_s = getCompColor(col_3_hl) * 1.0;
        col_4_s = vec3(0.0);
 
        vec3 color_01 = vec3(1.0,1.0,1.0);
        vec3 color_03 = vec3(0.0,0.0,1.0);
        
        vec3 light_0 = mix(mix(col_0_s, col_0_hl, pass_1.r), mix(col_1_s, col_1_hl, pass_1.g ), clamp(param_0 * 8.0, 0.0, 1.0));
        vec3 light_1 = mix(light_0, mix(col_2_s, col_2_hl, pass_1.b ), clamp((param_0 * 8.0) - 1.0, 0.0, 1.0));
        vec3 light_2 = mix(light_1, mix(col_3_s, col_3_hl, pass_2.r ), clamp((param_0 * 8.0) - 2.0, 0.0, 1.0));
        vec3 light_3 = mix(light_2,  mix(col_4_s, col_4_hl, pass_2.g ), clamp((param_0 * 8.0) - 3.0, 0.0, 1.0));
        vec3 light_4 = mix(light_3, mix(col_3_s, col_3_hl, pass_2.b ), clamp((param_0 * 8.0) - 4.0, 0.0, 1.0));
        vec3 light_5 = mix(light_4, mix(col_2_s, col_2_hl, pass_3.r ), clamp((param_0 * 8.0) - 5.0, 0.0, 1.0));
        vec3 light_6 = mix(light_5, mix(col_1_s, col_1_hl, pass_3.g), clamp((param_0 * 8.0) - 6.0, 0.0, 1.0));
        vec3 light_7 = mix(light_6, mix(col_0_s, col_0_hl, pass_3.b), clamp((param_0 * 8.0) - 7.0, 0.0, 1.0));
        
        vec3 result = clamp(vec3(pass_0.rgb) + light_7, 0.0, 1.0);

        gl_FragColor = vec4(result, 1.0);

    }`,
};

export { shader01, shader02 };
