//--------------------------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------------------------
import { Mesh, MeshBasicMaterial, OrthographicCamera, PerspectiveCamera, PlaneBufferGeometry, Scene, Vector3, WebGLRenderer } from "three";
import Stats from "three/examples/jsm/libs/stats.module";
import { GUI } from "lil-gui";

import { IData } from "../../data-feed";

export default class WglBaseScene {
    name: string;
    scene: Scene = new Scene();
    camera: OrthographicCamera | PerspectiveCamera;
    quad: Mesh;
    width: number;
    height: number;
    renderer: WebGLRenderer;
    onReady: () => void;
    stats: Stats;
    gui: GUI;

    constructor(name: string, width: number, height: number, renderer: WebGLRenderer, onReady: () => void) {
        this.name = name;
        this.camera = new OrthographicCamera(width / -2, width / 2, height / 2, height / -2, -10, 10);
        this.renderer = renderer;
        this.quad = this.createQuad();
        this.width = width;
        this.height = height;
        this.onReady = onReady;

        this.stats = Stats();
        this.renderer.domElement.parentElement.appendChild(this.stats.dom);

        this.gui = new GUI();
        this.gui.domElement.style.top = "294px";
    }

    updateData = (data: IData) => {};

    createQuad = () => {
        const plane = new PlaneBufferGeometry(1860, 292);
        plane.computeTangents();
        const material = new MeshBasicMaterial();
        const mesh = new Mesh(plane, material);
        this.scene.add(mesh);
        return mesh;
    };

    getScene = () => {
        return this.scene;
    };

    getCamera = () => {
        return this.camera;
    };

    update = () => {};
}
