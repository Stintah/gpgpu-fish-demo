//--------------------------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------------------------
import {
    AmbientLight,
    AmbientLightProbe,
    BufferAttribute,
    BufferGeometry,
    Color,
    DataTexture,
    DirectionalLight,
    EquirectangularReflectionMapping,
    Fog,
    HalfFloatType,
    HemisphereLight,
    LinearEncoding,
    LinearFilter,
    Mesh,
    MeshPhysicalMaterial,
    Object3D,
    PCFSoftShadowMap,
    PerspectiveCamera,
    RepeatWrapping,
    Shader,
    ShaderMaterial,
    SpotLight,
    sRGBEncoding,
    Texture,
    TextureLoader,
    UniformsUtils,
    Vector3,
    VideoTexture,
    WebGLRenderer,
} from "three";
import WglBaseScene from "./wgl-base-scene";
import { WglUtils } from "../utils/wgl-utils";
import { GPUComputationRenderer, Variable } from "three/examples/jsm/misc/GPUComputationRenderer";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { shader_position, shader_velocity } from "./shaders/shaders-gpgpu";

interface IParameters {
    speed: number;
    target_x: number;
    seperation_dist: number;
    alignment_dist: number;
    cohesion_dist: number;
}

export default class WglFlockingFish extends WglBaseScene {
    sim_width: number = 64;
    fish_count: number;

    fish_geometry: BufferGeometry = new BufferGeometry();
    fish_material: MeshPhysicalMaterial = new MeshPhysicalMaterial({ color: "white" });
    fish_mesh: Mesh | null = null;
    shader_material: Shader | ShaderMaterial | null = null;
    vertex_per_fish: number = 0;

    texture_env: Texture | null = null;

    last: number = performance.now();

    bounds: number = 50;
    bounds_half: number;

    gpu_compute: GPUComputationRenderer | null = null;

    velocity_variable: Variable | null = null;
    prev_velocity_variable: Variable | null = null;
    position_variable: Variable | null = null;
    position_uniforms: any = null;
    velocity_uniforms: any = null;

    parameters: IParameters = {
        speed: 2.0,
        target_x: 0.0,
        seperation_dist: 2.5,
        alignment_dist: 4.1,
        cohesion_dist: 20.0,
    };

    constructor(name: string, width: number, height: number, renderer: WebGLRenderer, onReady: () => void) {
        super(name, width, height, renderer, onReady);
        this.fish_count = this.sim_width * this.sim_width;
        this.bounds_half = this.bounds / 2;

        this.gui.add(this.parameters, "speed", 1.0, 4.0);
        this.gui.add(this.parameters, "target_x", -50, 50.0);
        this.gui.add(this.parameters, "seperation_dist", 1.0, 20.0);
        this.gui.add(this.parameters, "alignment_dist", 1.0, 20.0);
        this.gui.add(this.parameters, "cohesion_dist", 1.0, 20.0);

        this.init(onReady);
    }
    init = (onReady: () => void) => {
        this.quad.visible = false;
        this.camera = new PerspectiveCamera(20, this.width / this.height, 0.1, 500);
        this.camera.position.set(0, 0, 50);
        this.camera.lookAt(new Vector3());

        const light = new AmbientLight(new Color(1, 1, 1), 1000.0);
        //light.castShadow = true;

        //light.target = new Object3D();
        //light.distance = 100;
        //light.angle = Math.PI / 2;
        //light.position.y = 1.75;
        //light.position.x = 10;
        //light.position.z = 5.0;

        const hemi_light = new AmbientLightProbe("white", 3);
        //hemi_light.intensity = 1.0;
        this.scene.add(hemi_light);

        //this.renderer.physicallyCorrectLights = true;
        //this.renderer.shadowMap.enabled = true;
        //this.renderer.shadowMap.type = PCFSoftShadowMap;

        const bg = new Color(0.0, 0.0, 0.0);
        this.scene.background = bg;
        const fog_color = new Color(0.0, 0.0, 0.0);
        this.scene.fog = new Fog(fog_color, 5, 100);

        const gltf = "./models/fish_model.gltf";

        new GLTFLoader().load(gltf, (gltf) => {
            gltf.scene.traverse((child) => {
                if (child.name === "Fish") {
                    const geometry = (child as Mesh).geometry;

                    const texture_size = WglUtils.nextPowerOf2(geometry.getAttribute("position").count);
                    this.vertex_per_fish = geometry.getAttribute("position").count;

                    const vertices = [],
                        color = [],
                        reference = [],
                        seeds = [],
                        indices = [],
                        normals = [],
                        uvs = [];
                    const totalVertices = geometry.getAttribute("position").count * 3 * this.fish_count;

                    for (let i = 0; i < totalVertices; i++) {
                        const f_index = i % (geometry.getAttribute("position").count * 3);

                        vertices.push(geometry.getAttribute("position").array[f_index]);
                        normals.push(geometry.getAttribute("normal").array[f_index]);
                    }

                    const totalVertices_color = geometry.getAttribute("position").count * 4 * this.fish_count;
                    for (let i = 0; i < totalVertices_color; i++) {
                        const f_index_color = i % (geometry.getAttribute("position").count * 4);
                        color.push(geometry.getAttribute("color").array[f_index_color]);
                    }

                    const totalVertices_uv = geometry.getAttribute("position").count * 2 * this.fish_count;
                    for (let i = 0; i < totalVertices_uv; i++) {
                        const f_index_uv = i % (geometry.getAttribute("position").count * 2);
                        uvs.push(geometry.getAttribute("uv").array[f_index_uv]);
                    }

                    let r = Math.random();
                    for (let i = 0; i < geometry.getAttribute("position").count * this.fish_count; i++) {
                        const f_index = i % geometry.getAttribute("position").count;
                        const bird = Math.floor(i / geometry.getAttribute("position").count);
                        if (f_index === 0) r = Math.random();
                        const j = ~~bird;
                        const x = (j % this.sim_width) / this.sim_width;
                        const y = ~~(j / this.sim_width) / this.sim_width;
                        reference.push(x, y, f_index / texture_size, 60 / texture_size);

                        seeds.push(bird, r, Math.random(), Math.random());
                    }

                    if (geometry.index) {
                        for (let i = 0; i < geometry.index.array.length * this.fish_count; i++) {
                            const offset = Math.floor(i / geometry.index.array.length) * geometry.getAttribute("position").count;
                            indices.push(geometry.index.array[i % geometry.index.array.length] + offset);
                        }
                    }

                    this.fish_geometry.setAttribute("position", new BufferAttribute(new Float32Array(vertices), 3));
                    this.fish_geometry.setAttribute("uv", new BufferAttribute(new Float32Array(uvs), 2));
                    this.fish_geometry.setAttribute("normal", new BufferAttribute(new Float32Array(normals), 3));
                    this.fish_geometry.setAttribute("birdColor", new BufferAttribute(new Float32Array(color), 4));
                    this.fish_geometry.setAttribute("color", new BufferAttribute(new Float32Array(color), 4));
                    this.fish_geometry.setAttribute("reference", new BufferAttribute(new Float32Array(reference), 4));
                    this.fish_geometry.setAttribute("seeds", new BufferAttribute(new Float32Array(seeds), 4));

                    this.fish_geometry.setIndex(indices);
                    this.initComputeRenderer();
                    this.initFish(onReady);
                }
            });
        });
        //onReady();
    };

    initComputeRenderer = () => {
        this.gpu_compute = new GPUComputationRenderer(this.sim_width, this.sim_width, this.renderer);

        if (WglUtils.isSafari()) {
            this.gpu_compute.setDataType(HalfFloatType);
        }

        const dtPosition = this.gpu_compute.createTexture();
        const dtVelocity = this.gpu_compute.createTexture();
        const dtPrevVelocity = this.gpu_compute.createTexture();
        this.fillPositionTexture(dtPosition);
        this.fillVelocityTexture(dtVelocity);
        this.fillVelocityTexture(dtPrevVelocity);

        this.velocity_variable = this.gpu_compute.addVariable("textureVelocity", shader_velocity.fragmentShader, dtVelocity);
        this.prev_velocity_variable = this.gpu_compute.addVariable("texturePrevVelocity", shader_velocity.fragmentShader, dtPrevVelocity);
        this.position_variable = this.gpu_compute.addVariable("texturePosition", shader_position.fragmentShader, dtPosition);

        this.gpu_compute.setVariableDependencies(this.velocity_variable, [this.position_variable, this.velocity_variable, this.prev_velocity_variable]);
        this.gpu_compute.setVariableDependencies(this.prev_velocity_variable, [this.position_variable, this.velocity_variable, this.prev_velocity_variable]);
        this.gpu_compute.setVariableDependencies(this.position_variable, [this.position_variable, this.velocity_variable, this.prev_velocity_variable]);

        this.position_uniforms = this.position_variable.material.uniforms;
        this.velocity_uniforms = this.velocity_variable.material.uniforms;

        this.position_uniforms["speed"] = { value: 2.0 };
        this.position_uniforms["time"] = { value: 1.0 };
        this.position_uniforms["delta"] = { value: 0.0 };
        this.velocity_uniforms["target_x"] = { value: 0.0 };
        this.velocity_uniforms["time"] = { value: 1.0 };
        this.velocity_uniforms["delta"] = { value: 0.0 };
        this.velocity_uniforms["testing"] = { value: 1.0 };
        this.velocity_uniforms["separationDistance"] = { value: 2.5 };
        this.velocity_uniforms["alignmentDistance"] = { value: 4.1 };
        this.velocity_uniforms["cohesionDistance"] = { value: 25.5 };
        this.velocity_uniforms["freedomFactor"] = { value: 1.0 };
        this.velocity_uniforms["predator"] = { value: new Vector3(1.0, 1.0, 1.0) };
        this.prev_velocity_variable.material.defines.bounds = this.bounds.toFixed(2);
        this.prev_velocity_variable.material.defines.sim_width = this.sim_width.toFixed(2);
        this.velocity_variable.material.defines.bounds = this.bounds.toFixed(2);
        this.velocity_variable.material.defines.sim_width = this.sim_width.toFixed(2);
        this.position_variable.material.defines.sim_width = this.sim_width.toFixed(2);

        this.velocity_variable.wrapS = RepeatWrapping;
        this.velocity_variable.wrapT = RepeatWrapping;
        this.prev_velocity_variable.wrapS = RepeatWrapping;
        this.prev_velocity_variable.wrapT = RepeatWrapping;
        this.position_variable.wrapS = RepeatWrapping;
        this.position_variable.wrapT = RepeatWrapping;

        const error = this.gpu_compute.init();

        if (error !== null) {
            console.error(error);
        }
    };

    initFish = (onReady: () => void) => {
        const geometry = this.fish_geometry;

        const texture_loader = new TextureLoader();

        const normal_map = texture_loader.load("./models/fish_normals.png");
        const base_color_map = texture_loader.load("./models/fish_baseColor.png");
        const spec_map = texture_loader.load("./models/fish_spec.png");
        spec_map.encoding = LinearEncoding;
        const roughness_map = texture_loader.load("./models/fish_normals.png");
        roughness_map.encoding = LinearEncoding;
        /*
        const env_map = texture_loader.load("./models/abandoned_waterworks_4k.png");
        env_map.mapping = EquirectangularReflectionMapping;
        env_map.encoding = sRGBEncoding;
        */

        //this.scene.background = env_map;

        normal_map.flipY = false;
        base_color_map.flipY = false;
        spec_map.flipY = false;
        roughness_map.flipY = false;

        this.fish_material.color = new Color(1.0, 1.0, 1.0);
        this.fish_material.roughness = 0.75;
        //this.material.vertexColors = true;
        this.fish_material.transmission = 0.5;

        this.fish_material.normalMap = normal_map;
        this.fish_material.map = base_color_map;
        //this.fish_material.metalnessMap = spec_map;
        //this.fish_material.metalness = 1.0;
        this.fish_material.roughnessMap = roughness_map;

        //this.fish_material.envMap = env_map;
        //this.fish_material.envMapIntensity = 1.25;

        //geometry.setDrawRange(0, this.vertex_per_fish * this.birds);

        this.fish_material.onBeforeCompile = (shader) => {
            shader.uniforms.texturePrevVelocity = { value: null };
            shader.uniforms.texturePosition = { value: null };
            shader.uniforms.textureVelocity = { value: null };
            shader.uniforms.textureAnimation = { value: null };
            shader.uniforms.time = { value: 1.0 };
            shader.uniforms.size = { value: 1.0 };
            shader.uniforms.delta = { value: 0.0 };
            let token = "#define STANDARD";

            let insert = /* glsl */ `
                    attribute vec4 reference;
                    attribute vec4 seeds;
                    attribute vec4 birdColor;
                    uniform sampler2D texturePosition;
                    uniform sampler2D textureVelocity;
                    uniform sampler2D texturePrevVelocity;
                    uniform sampler2D textureAnimation;
                    uniform float size;
                    uniform float time;
                    uniform float delta;

                    const float PI = 3.141592653589793;
                    const float PI_half = PI * 0.5;
	                const float PI_2 = PI * 2.0;

                    float local_angle_x = 0.0;
                    float local_angle_z = 0.0;

                    float angleVec2(vec2 v0, vec2 v1) {
                        float angle = acos(dot(v0,v1));
                        angle -= PI_half;
                        
                        return angle;
                    }

                    float signedAngle(vec3 v0, vec3 v1) {
                        float angle = acos(dot(v0,v1));
                        angle -= PI * 0.5;
                        vec3 cross_product = cross(v0, v1);
                        vec3 N = vec3(0.0, 1.0, 0.0);
                        if (dot(N, cross_product) < 0.0) {
                            // Or > 0
                            //angle = -angle;
                        }
                        return angle;
                    }
                `;

            shader.vertexShader = shader.vertexShader.replace(token, token + insert);

            token = "#include <begin_vertex>";

            insert = /* glsl */ `
                    vec4 tmpPos = texture2D( texturePosition, reference.xy );
                    vec3 pos = tmpPos.xyz;
                    vec3 velocity = normalize(texture2D( textureVelocity, reference.xy ).xyz);
                    //vec3 aniPos = texture2D( textureAnimation, vec2( reference.z, mod( time + ( seeds.x ) * ( ( 0.0004 + seeds.y / 10000.0) + normalize( velocity ) / 20000.0 ), reference.w ) ) ).xyz;
                    vec3 newPosition = position;

                    vec3 prevVelocity = normalize(texture2D( texturePrevVelocity, reference.xy ).xyz);
                    
                    vec3 velocityDelta = prevVelocity - velocity;

                    
                    //float movement = sin((time*wiggle_speed)+seeds.x);
                    //float movement_02 = sin((time*(wiggle_speed))+seeds.x);
                    //float movement_03 = sin((time*(wiggle_speed*0.5))+seeds.y) *0.25;
                    //float wiggle_x = (mix(movement, movement * -1.0, birdColor.r ) + mix(movement_02*-1.0, movement_02, birdColor.g )) * wiggle_amount;
                    //float wiggle_y = mix(movement_03*-1.0, movement_03, birdColor.b ) * wiggle_amount;

                    vec3 vert_anim_position = vec3(0.0) * 1.0;

                    float bend_amount = 0.25;

                    local_angle_x = mix(0.0, (angleVec2(vec2(0.0,1.0), normalize(vec2(velocityDelta.xz)))), birdColor.r) * bend_amount;
                    local_angle_z = mix(0.0, (angleVec2(vec2(1.0,0.0), normalize(vec2(-velocityDelta.yz)))), birdColor.r) * bend_amount;
                    
                    float wiggle_amount = 0.05;
                    float wiggle_speed = 6.0;
                    float wiggle = sin((time+seeds.x)*wiggle_speed) * wiggle_amount;
                    local_angle_x += wiggle + mix(0.0, wiggle, birdColor.r);
                    local_angle_z += wiggle + mix(0.0, wiggle, birdColor.r);
                    

                    //angle = acos(dotProduct(Va.normalize(), Vb.normalize()));

                    //float local_angle_x = signedAngle(velocity, prevVelocity) *0.25;
                    mat3 localY = mat3( cos(local_angle_x), 0, sin(local_angle_x), 0    , 1, 0     , -sin(local_angle_x), 0, cos(local_angle_x) );

                    mat3 localZ = mat3( cos(local_angle_z), sin(local_angle_z), 0, -sin(local_angle_z), cos(local_angle_z), 0, 0, 0, 1 );
                    //mat3 matz =  mat3( cosrz , sinrz, 0, -sinrz, cosrz, 0, 0     , 0    , 1 );

                    //newPosition = mat3( modelMatrix  )  * ( newPosition );

                    newPosition = mat3( modelMatrix ) * (newPosition + vert_anim_position);
                    //newPosition *= size + seeds.y * size * 1.0;
                    velocity.z *= -1.;
                    //prevVelocity.z *= -1.;
                    float xz = length( velocity.xz );
                    //float xz = length( mix(prevVelocity.xz, velocity.xz, birdColor.r) );
                    float xyz = 1.;
                    float x = sqrt( 1. - velocity.y * velocity.y );
                    //float x = sqrt( 1. - mix(prevVelocity.y, velocity.y, birdColor.r) * mix(prevVelocity.y, velocity.y, birdColor.r) );
                    float cosry = velocity.x / xz;
                    //float cosry = mix(prevVelocity.x, velocity.x, birdColor.r)/xz;
                    float sinry = velocity.z / xz;
                    //float sinry = mix(prevVelocity.z, velocity.z, birdColor.r)/xz;
                    float cosrz = x / xyz;
                    float sinrz = velocity.y / xyz;
                    //float sinrz = mix(prevVelocity.y, velocity.y, birdColor.r) / xyz;
                    
                    //mat3 localY = mat3( mix(0.0, cosry, birdColor.r), 0,  mix(0.0, -sinry, birdColor.r), 0    , 1, 0     , mix(0.0, sinry, birdColor.r), 0, mix(0.0, cosry, birdColor.r) );
                    mat3 maty =  mat3( cosry, 0, -sinry, 0    , 1, 0     , sinry, 0, cosry );
                    mat3 matz =  mat3( cosrz , sinrz, 0, -sinrz, cosrz, 0, 0     , 0    , 1 );
                    mat3 full_transformation =  maty * matz;
                    newPosition =  full_transformation * newPosition * localY * localZ;
                    
                    newPosition += pos;
                    vec3 transformed = vec3( newPosition );
    
                    vNormal = full_transformation * vNormal;
    
                `;

            shader.vertexShader = shader.vertexShader.replace(token, insert);

            this.shader_material = shader;

            console.log("ready");
        };

        this.fish_mesh = new Mesh(geometry, this.fish_material);

        this.fish_mesh.rotation.y = Math.PI / 2;

        //this.fish_mesh.castShadow = true;
        //this.fish_mesh.receiveShadow = true;

        this.scene.add(this.fish_mesh);
        onReady();
        //this.renderer.compile(this.scene, this.camera);
    };

    fillPositionTexture = (texture: DataTexture) => {
        const array = texture.image.data;

        for (let k = 0, kl = array.length; k < kl; k += 4) {
            const x = Math.random() * this.bounds - this.bounds_half;
            const y = Math.random() * this.bounds - this.bounds_half;
            const z = Math.random() * this.bounds - this.bounds_half;

            array[k + 0] = x;
            array[k + 1] = y;
            array[k + 2] = z;
            array[k + 3] = 1;
        }
    };

    fillVelocityTexture = (texture: DataTexture) => {
        const array = texture.image.data;

        for (let k = 0, kl = array.length; k < kl; k += 4) {
            const x = Math.random() - 0.5;
            const y = Math.random() - 0.5;
            const z = Math.random() - 0.5;

            array[k + 0] = x * 10;
            array[k + 1] = y * 10;
            array[k + 2] = z * 10;
            array[k + 3] = 1;
        }
    };

    update = () => {
        this.stats.begin();
        /*
        this.current_data.param_0 = WglUtils.moveTowards(this.current_data.param_0, this.data.param_0, 0.005);
        this.shader_material.uniforms.param_0.value = this.current_data.param_0;
        this.shader_material.uniforms.param_1.value = this.data.param_1;
        this.shader_material.uniforms.param_2.value = this.data.param_2;
        this.shader_material.uniforms.param_3.value = this.data.param_3;
        this.shader_material.uniforms.param_4.value = this.data.param_4;
        this.shader_material.uniforms.param_5.value = this.data.param_5;
        this.shader_material.uniforms.param_6.value = this.data.param_6;
*/
        const now = performance.now();
        let delta = (now - this.last) / 1000;

        if (delta > 1) delta = 1; // safety cap on large deltas
        this.last = now;
        this.position_uniforms["speed"].value = this.parameters.speed;
        this.position_uniforms["time"].value = now;
        this.position_uniforms["delta"].value = delta;
        this.velocity_uniforms["time"].value = now;
        this.velocity_uniforms["delta"].value = delta;

        this.velocity_uniforms["target_x"].value = this.parameters.target_x;
        this.velocity_uniforms["separationDistance"].value = this.parameters.seperation_dist;
        this.velocity_uniforms["alignmentDistance"].value = this.parameters.alignment_dist;
        this.velocity_uniforms["cohesionDistance"].value = this.parameters.cohesion_dist;

        if (this.shader_material) this.shader_material.uniforms["time"].value = now / 1000;
        if (this.shader_material) this.shader_material.uniforms["delta"].value = delta;

        /*
        this.velocity_uniforms["predator"].value.set((0.5 * this.mouse_position.x) / this.width / 2, (-0.5 * this.mouse_position.y) / this.height / 2, 0);

        this.mouse_position.x = 10000;
        this.mouse_position.y = 10000;
        */

        if (this.gpu_compute && this.position_variable && this.velocity_variable && this.shader_material) {
            this.shader_material.uniforms["texturePrevVelocity"].value = (this.gpu_compute.getCurrentRenderTarget(this.position_variable) as any).texture;
            this.gpu_compute.compute();

            this.shader_material.uniforms["texturePosition"].value = (this.gpu_compute.getCurrentRenderTarget(this.position_variable) as any).texture;
            this.shader_material.uniforms["textureVelocity"].value = (this.gpu_compute.getCurrentRenderTarget(this.velocity_variable) as any).texture;
        }

        this.renderer.render(this.scene, this.camera);

        this.stats.end();
    };
}
