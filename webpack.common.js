const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: {
        app: "./src/js/app.ts",
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
    },
    devServer: {
        static: "/dist/",
    },
    module: {
        rules: [
            {
                test: /\.html$/i,
                loader: "html-loader",
            },
            {
                test: /\.ts?$/,
                exclude: /node_modules/,
                use: [{ loader: "ts-loader" }],
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [require.resolve("style-loader"), require.resolve("css-loader")],
            },
            {
                test: /\.mp4$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "url-loader",
                    },
                    {
                        loader: "file-loader",
                    },
                ],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Fish Demo",
            meta: {
                viewport: "width=device-width, initial-scale=1.0, shrink-to-fit=no, user-scalable=no",
            },
        }),
        new CopyPlugin({
            patterns: [{ from: "public" }],
        }),
    ],
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist"),
        clean: true,
    },
};
